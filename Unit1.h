//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include <ExtCtrls.hpp>
#include <Dialogs.hpp>
#include <ImgList.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TStatusBar *StatusBar1;
        TMainMenu *MainMenu1;
        TMenuItem *N1;
        TMenuItem *N2;
        TMenuItem *N3;
        TMenuItem *N4;
        TMenuItem *csv1;
        TMenuItem *json1;
        TMenuItem *xml1;
        TMenuItem *N5;
        TMenuItem *N6;
        TMenuItem *N7;
        TMenuItem *N8;
        TMenuItem *N9;
        TMenuItem *N10;
        TMenuItem *N11;
        TMenuItem *N12;
        TMenuItem *N13;
        TMenuItem *csv2;
        TMenuItem *json2;
        TMenuItem *N14;
        TMenuItem *xml2;
        TPanel *Panel1;
        TMemo *Memo1;
        TListBox *ListBox1;
        TMemo *Memo2;
        TMemo *Memo3;
        TMenuItem *N15;
        TMenuItem *N16;
        TOpenDialog *OpenDialog1;
        TSaveDialog *SaveDialog1;
        TMemo *Memo4;
        TImageList *ImageList1;
        TToolBar *ToolBar1;
        TToolButton *ToolButton13;
        TToolButton *ToolButton5;
        TToolButton *ToolButton6;
        TToolButton *ToolButton1;
        TToolButton *ToolButton17;
        TToolButton *ToolButton3;
        TToolButton *ToolButton7;
        TToolButton *ToolButton20;
        TToolButton *ToolButton21;
        TToolButton *ToolButton22;
        TToolButton *ToolButton11;
        TToolButton *ToolButton24;
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall FormResize(TObject *Sender);
        void __fastcall N6Click(TObject *Sender);
        void __fastcall csv2Click(TObject *Sender);
        void __fastcall json2Click(TObject *Sender);
        void __fastcall N14Click(TObject *Sender);
        void __fastcall xml2Click(TObject *Sender);
        void __fastcall N16Click(TObject *Sender);
        void __fastcall csv1Click(TObject *Sender);
        void __fastcall json1Click(TObject *Sender);
        void __fastcall xml1Click(TObject *Sender);
        void __fastcall N12Click(TObject *Sender);
        void __fastcall N8Click(TObject *Sender);
        void __fastcall N9Click(TObject *Sender);
        void __fastcall ListBox1Click(TObject *Sender);
        void __fastcall N10Click(TObject *Sender);
        void __fastcall N7Click(TObject *Sender);
        void __fastcall ToolButton11Click(TObject *Sender);
        void __fastcall ToolButton3Click(TObject *Sender);
        void __fastcall ToolButton7Click(TObject *Sender);
        void __fastcall ToolButton5Click(TObject *Sender);
        void __fastcall ToolButton6Click(TObject *Sender);
        void __fastcall ToolButton1Click(TObject *Sender);
        void __fastcall ToolButton14Click(TObject *Sender);
        void __fastcall ToolButton15Click(TObject *Sender);
        void __fastcall ToolButton16Click(TObject *Sender);
        void __fastcall ToolButton18Click(TObject *Sender);
        void __fastcall ToolButton19Click(TObject *Sender);
        void __fastcall ToolButton23Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;

struct _Types{
        String TypeName;
        String Property[128];
        String PropertyType[128];
};

extern struct _Types dataTypes[8];

extern int    offset;


//---------------------------------------------------------------------------
#endif
