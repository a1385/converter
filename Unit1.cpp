//---------------------------------------------------------------------------

#include <vcl.h>
#include <String.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit2.h"
#include "Unit3.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;

struct _Types dataTypes[8];
int    offset = 100;

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{

}
//---------------------------------------------------------------------------



//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
        String srcData[128];
        String srcType;
        int    dotPos;
        String sTmp;
        String baseString;

        String Tag;
        String Offset;

        for(int i=0, j=0; i<ListBox1->Items->Count; i++)
        {
                if(ListBox1->Selected[i] == true)
                {
                        srcData[j] = ListBox1->Items->Strings[i];
                        j++;
                }
        }

        Memo2->Lines->Add("| tag | offset |");
        for(int i=0; i<128; i++)
        {
                sTmp = srcData[i];
                if(sTmp == "") break;

                dotPos = sTmp.Pos(";");
                srcType = sTmp.SubString(dotPos + 1, sTmp.Length()- dotPos - 1);
                baseString = sTmp.SubString(0, dotPos - 1);

                for(int j=0; j<8; j++)
                {
                        if(dataTypes[j].TypeName == "") break;

                        if(srcType == dataTypes[j].TypeName)
                        {
                                for(int k=0; k<128; k++)
                                {
                                        if(dataTypes[j].Property[k] == "") break;
                                        Memo2->Lines->Add("| " + baseString + "." + dataTypes[j].Property[k] + " | " + offset + " |");
                                        if(dataTypes[j].PropertyType[k] == "bool")   offset = offset + 1;
                                        if(dataTypes[j].PropertyType[k] == "int")    offset = offset + 4;
                                        if(dataTypes[j].PropertyType[k] == "double") offset = offset + 8;
                                }
                        }
                }
        }

        Memo3->Lines->Add("<root>");

        for(int i=1; i<Memo2->Lines->Count; i++)
        {
                sTmp = Memo2->Lines->Strings[i];
                sTmp = sTmp.SubString(3, sTmp.Length() - 4);
                dotPos = sTmp.Pos("|");
                Tag = sTmp.SubString(0, dotPos - 2);
                Offset = sTmp.SubString(dotPos + 2, sTmp.Length() - dotPos);

                Memo3->Lines->Add("     <item Binding=\"Introduced\">");
                Memo3->Lines->Add("             <node-path>" + Tag + "</node-path>");
                Memo3->Lines->Add("             <address>" + Offset + "</address>");
                Memo3->Lines->Add("     </item>");
        }

        Memo3->Lines->Add("</root>");

        Memo3->Lines->SaveToFile("G:\\result.xml");

}
//---------------------------------------------------------------------------



void __fastcall TForm1::FormResize(TObject *Sender)
{
        this->Width  = 1024;
        this->Height = 800;
}
//---------------------------------------------------------------------------















void __fastcall TForm1::N6Click(TObject *Sender)
{
        this->Close();
}
//---------------------------------------------------------------------------






void __fastcall TForm1::csv2Click(TObject *Sender)
{
        ListBox1->Left = 0;
        ListBox1->Top = 0;
        ListBox1->Width = 1009;
        ListBox1->Height = 657;
        ListBox1->Visible = true;

        Memo1->Left = 0;
        Memo1->Top = 0;
        Memo1->Width = 1009;
        Memo1->Height = 657;
        Memo1->Visible = false;

        Memo2->Left = 0;
        Memo2->Top = 0;
        Memo2->Width = 1009;
        Memo2->Height = 657;
        Memo2->Visible = false;

        Memo3->Left = 0;
        Memo3->Top = 0;
        Memo3->Width = 1009;
        Memo3->Height = 657;
        Memo3->Visible = false;

        csv2->Checked  = true;
        json2->Checked = false;
        N14->Checked   = false;
        xml2->Checked  = false;
        N16->Checked   = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::json2Click(TObject *Sender)
{
        ListBox1->Left = 0;
        ListBox1->Top = 0;
        ListBox1->Width = 1009;
        ListBox1->Height = 657;
        ListBox1->Visible = false;

        Memo1->Left = 0;
        Memo1->Top = 0;
        Memo1->Width = 1009;
        Memo1->Height = 657;
        Memo1->Visible = true;

        Memo2->Left = 0;
        Memo2->Top = 0;
        Memo2->Width = 1009;
        Memo2->Height = 657;
        Memo2->Visible = false;

        Memo3->Left = 0;
        Memo3->Top = 0;
        Memo3->Width = 1009;
        Memo3->Height = 657;
        Memo3->Visible = false;

        csv2->Checked  = false;
        json2->Checked = true;
        N14->Checked   = false;
        xml2->Checked  = false;
        N16->Checked   = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::N14Click(TObject *Sender)
{
        ListBox1->Left = 0;
        ListBox1->Top = 0;
        ListBox1->Width = 1009;
        ListBox1->Height = 657;
        ListBox1->Visible = false;

        Memo1->Left = 0;
        Memo1->Top = 0;
        Memo1->Width = 1009;
        Memo1->Height = 657;
        Memo1->Visible = false;

        Memo2->Left = 0;
        Memo2->Top = 0;
        Memo2->Width = 1009;
        Memo2->Height = 657;
        Memo2->Visible = true;

        Memo3->Left = 0;
        Memo3->Top = 0;
        Memo3->Width = 1009;
        Memo3->Height = 657;
        Memo3->Visible = false;

        csv2->Checked  = false;
        json2->Checked = false;
        N14->Checked   = true;
        xml2->Checked  = false;
        N16->Checked   = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::xml2Click(TObject *Sender)
{
        ListBox1->Left = 0;
        ListBox1->Top = 0;
        ListBox1->Width = 1009;
        ListBox1->Height = 657;
        ListBox1->Visible = false;

        Memo1->Left = 0;
        Memo1->Top = 0;
        Memo1->Width = 1009;
        Memo1->Height = 657;
        Memo1->Visible = false;

        Memo2->Left = 0;
        Memo2->Top = 0;
        Memo2->Width = 1009;
        Memo2->Height = 657;
        Memo2->Visible = false;

        Memo3->Left = 0;
        Memo3->Top = 0;
        Memo3->Width = 1009;
        Memo3->Height = 657;
        Memo3->Visible = true;

        
        csv2->Checked  = false;
        json2->Checked = false;
        N14->Checked   = false;
        xml2->Checked  = true;
        N16->Checked   = false;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::N16Click(TObject *Sender)
{

        if(N16->Checked == false)
        {
                ListBox1->Left = 0;
                ListBox1->Top = 0;
                ListBox1->Width = 500;
                ListBox1->Height = 325;
                ListBox1->Visible = true;

                Memo1->Left = 510;
                Memo1->Top = 0;
                Memo1->Width = 500;
                Memo1->Height = 325;
                Memo1->Visible = true;

                Memo2->Left = 0;
                Memo2->Top = 330;
                Memo2->Width = 500;
                Memo2->Height = 325;
                Memo2->Visible = true;

                Memo3->Left = 510;
                Memo3->Top = 330;
                Memo3->Width = 500;
                Memo3->Height = 325;
                Memo3->Visible = true;

                csv2->Checked  = false;
                json2->Checked = false;
                N14->Checked   = false;
                xml2->Checked  = false;
                N16->Checked   = true;
        }
        else
        {
                ListBox1->Left = 0;
                ListBox1->Top = 0;
                ListBox1->Width = 1009;
                ListBox1->Height = 657;
                ListBox1->Visible = true;

                Memo1->Left = 0;
                Memo1->Top = 0;
                Memo1->Width = 1009;
                Memo1->Height = 657;
                Memo1->Visible = false;

                Memo2->Left = 0;
                Memo2->Top = 0;
                Memo2->Width = 1009;
                Memo2->Height = 657;
                Memo2->Visible = false;

                Memo3->Left = 0;
                Memo3->Top = 0;
                Memo3->Width = 1009;
                Memo3->Height = 657;
                Memo3->Visible = false;

                csv2->Checked  = true;
                json2->Checked = false;
                N14->Checked   = false;
                xml2->Checked  = false;
                N16->Checked   = false;
        }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::csv1Click(TObject *Sender)
{
        String tmp;
        String tmp1;

        int    dotPos;

        OpenDialog1->FilterIndex = 1;
        OpenDialog1->FileName = "";
        OpenDialog1->Execute();
        
        if(OpenDialog1->FileName != "")
        {
                Memo2->Clear();
                Memo3->Clear();
                N12->Enabled = false;
                xml1->Enabled = false;
                ToolButton1->Enabled = false;
                ToolButton7->Enabled = false;

                Memo4->Lines->Clear();
                ListBox1->Clear();

                Memo4->Lines->LoadFromFile(OpenDialog1->FileName);

                for(int i=1; i<=Memo4->Lines->Count; i++)
                {
                        tmp = Memo4->Lines->Strings[i];
                        ListBox1->Items->Add(tmp);
                }

                ListBox1->Left = 0;
                ListBox1->Top = 0;
                ListBox1->Width = 1009;
                ListBox1->Height = 657;
                ListBox1->Visible = true;

                Memo1->Left = 0;
                Memo1->Top = 0;
                Memo1->Width = 1009;
                Memo1->Height = 657;
                Memo1->Visible = false;

                Memo2->Left = 0;
                Memo2->Top = 0;
                Memo2->Width = 1009;
                Memo2->Height = 657;
                Memo2->Visible = false;

                Memo3->Left = 0;
                Memo3->Top = 0;
                Memo3->Width = 1009;
                Memo3->Height = 657;
                Memo3->Visible = false;

                csv2->Checked  = true;
                json2->Checked = false;
                N14->Checked   = false;
                xml2->Checked  = false;
                N16->Checked   = false;
        }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::json1Click(TObject *Sender)
{
        String tmp;
        String tmp1;

        int    dotPos;
        
        OpenDialog1->FilterIndex = 2;
        OpenDialog1->FileName = "";
        OpenDialog1->Execute();

        if(OpenDialog1->FileName != "")
        {
                Memo2->Clear();
                Memo3->Clear();
                N12->Enabled = false;
                xml1->Enabled = false;
                ToolButton1->Enabled = false;
                ToolButton7->Enabled = false;

                Memo1->Clear();

                Memo1->Lines->LoadFromFile(OpenDialog1->FileName);

                for(int i=1, j=0; i<=Memo1->Lines->Count; i++)
                {
                        tmp = Memo1->Lines->Strings[i];

                        if(tmp.SubString(7,11) == "\"TypeName\":")
                        {
                                dotPos = tmp.Pos(",");
                                dataTypes[j].TypeName = tmp.SubString(20,dotPos - 21);
                                i = i + 2;
                                for(int k=i, l=0; k<=i + 128; k++, l++)
                                {
                                        tmp1 = Memo1->Lines->Strings[k];

                                        if(tmp1.SubString(7,1) == "}") break;
                                        dotPos = tmp1.Pos(":");
                                        dataTypes[j].Property[l] = tmp1.SubString(10, dotPos - 11);
                                        if(tmp1.SubString(dotPos + 2, 3) == "\"bo") dataTypes[j].PropertyType[l] = "bool";
                                        if(tmp1.SubString(dotPos + 2, 3) == "\"in") dataTypes[j].PropertyType[l] = "int";
                                        if(tmp1.SubString(dotPos + 2, 3) == "\"do") dataTypes[j].PropertyType[l] = "double";

                                }
                                j++;
                        }
                }

                ListBox1->Left = 0;
                ListBox1->Top = 0;
                ListBox1->Width = 1009;
                ListBox1->Height = 657;
                ListBox1->Visible = false;

                Memo1->Left = 0;
                Memo1->Top = 0;
                Memo1->Width = 1009;
                Memo1->Height = 657;
                Memo1->Visible = true;

                Memo2->Left = 0;
                Memo2->Top = 0;
                Memo2->Width = 1009;
                Memo2->Height = 657;
                Memo2->Visible = false;

                Memo3->Left = 0;
                Memo3->Top = 0;
                Memo3->Width = 1009;
                Memo3->Height = 657;
                Memo3->Visible = false;

                csv2->Checked  = false;
                json2->Checked = true;
                N14->Checked   = false;
                xml2->Checked  = false;
                N16->Checked   = false;
        }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::xml1Click(TObject *Sender)
{
        SaveDialog1->FilterIndex = 1;
        OpenDialog1->FileName = "";
        SaveDialog1->Execute();

        if(SaveDialog1->FileName != "")
                Memo3->Lines->SaveToFile(SaveDialog1->FileName + ".xml");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::N12Click(TObject *Sender)
{
        String srcData[128];
        String srcType;
        int    dotPos;
        String sTmp;
        String baseString;

        String Tag;
        String Offset;

        for(int i=0, j=0; i<ListBox1->Items->Count; i++)
        {
                if(ListBox1->Selected[i] == true)
                {
                        srcData[j] = ListBox1->Items->Strings[i];
                        j++;
                }
        }

        Memo2->Lines->Add("| tag | offset |");
        for(int i=0; i<128; i++)
        {
                sTmp = srcData[i];
                if(sTmp == "") break;

                dotPos = sTmp.Pos(";");
                srcType = sTmp.SubString(dotPos + 1, sTmp.Length()- dotPos - 1);
                baseString = sTmp.SubString(0, dotPos - 1);

                for(int j=0; j<8; j++)
                {
                        if(dataTypes[j].TypeName == "") break;

                        if(srcType == dataTypes[j].TypeName)
                        {
                                for(int k=0; k<128; k++)
                                {
                                        if(dataTypes[j].Property[k] == "") break;
                                        Memo2->Lines->Add("| " + baseString + "." + dataTypes[j].Property[k] + " | " + offset + " |");
                                        if(dataTypes[j].PropertyType[k] == "bool")   offset = offset + 1;
                                        if(dataTypes[j].PropertyType[k] == "int")    offset = offset + 4;
                                        if(dataTypes[j].PropertyType[k] == "double") offset = offset + 8;
                                }
                        }
                }
        }

        Memo3->Lines->Add("<root>");

        for(int i=1; i<Memo2->Lines->Count; i++)
        {
                sTmp = Memo2->Lines->Strings[i];
                sTmp = sTmp.SubString(3, sTmp.Length() - 4);
                dotPos = sTmp.Pos("|");
                Tag = sTmp.SubString(0, dotPos - 2);
                Offset = sTmp.SubString(dotPos + 2, sTmp.Length() - dotPos);

                Memo3->Lines->Add("     <item Binding=\"Introduced\">");
                Memo3->Lines->Add("             <node-path>" + Tag + "</node-path>");
                Memo3->Lines->Add("             <address>" + Offset + "</address>");
                Memo3->Lines->Add("     </item>");
        }

        Memo3->Lines->Add("</root>");

        ListBox1->Left = 0;
        ListBox1->Top = 0;
        ListBox1->Width = 1009;
        ListBox1->Height = 657;
        ListBox1->Visible = false;

        Memo1->Left = 0;
        Memo1->Top = 0;
        Memo1->Width = 1009;
        Memo1->Height = 657;
        Memo1->Visible = false;

        Memo2->Left = 0;
        Memo2->Top = 0;
        Memo2->Width = 1009;
        Memo2->Height = 657;
        Memo2->Visible = false;

        Memo3->Left = 0;
        Memo3->Top = 0;
        Memo3->Width = 1009;
        Memo3->Height = 657;
        Memo3->Visible = true;

        csv2->Checked  = false;
        json2->Checked = false;
        N14->Checked   = false;
        xml2->Checked  = true;
        N16->Checked   = false;

        xml1->Enabled = true;
        ToolButton1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::N8Click(TObject *Sender)
{
        if(N8->Checked == true)
        {
                StatusBar1->Visible = false;
                N8->Checked = false;
        }
        else
        {
                StatusBar1->Visible = true;
                N8->Checked = true;
        }
}
//---------------------------------------------------------------------------


void __fastcall TForm1::N9Click(TObject *Sender)
{
        if(N9->Checked == true)
        {
                ToolBar1->Visible = false;
                N9->Checked = false;
        }
        else
        {
                ToolBar1->Visible = true;
                N9->Checked = true;
        }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ListBox1Click(TObject *Sender)
{
        if((ListBox1->SelCount > 0) && (Memo1->Lines->Count > 0))
        {
                N12->Enabled = true;
                ToolButton7->Enabled = true;
        }
        else
        {
                N12->Enabled = false;
                ToolButton7->Enabled = false;
        }
}
//---------------------------------------------------------------------------



void __fastcall TForm1::N10Click(TObject *Sender)
{
        Form2->ShowModal();

}
//---------------------------------------------------------------------------

void __fastcall TForm1::N7Click(TObject *Sender)
{
        Form3->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ToolButton11Click(TObject *Sender)
{
        Form3->ShowModal();        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ToolButton3Click(TObject *Sender)
{
        Form2->ShowModal();        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ToolButton7Click(TObject *Sender)
{
        N12->Click();        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ToolButton5Click(TObject *Sender)
{
        csv1->Click();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ToolButton6Click(TObject *Sender)
{
        json1->Click();        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ToolButton1Click(TObject *Sender)
{
        xml1->Click();        
}
//---------------------------------------------------------------------------



void __fastcall TForm1::ToolButton14Click(TObject *Sender)
{
        csv1->Click();        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ToolButton15Click(TObject *Sender)
{
        json1->Click();        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ToolButton16Click(TObject *Sender)
{
        xml1->Click();        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ToolButton18Click(TObject *Sender)
{
        Form2->ShowModal();        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ToolButton19Click(TObject *Sender)
{
        N12->Click();        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ToolButton23Click(TObject *Sender)
{
        Form3->ShowModal();        
}
//---------------------------------------------------------------------------


